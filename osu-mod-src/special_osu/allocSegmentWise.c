#define BENCHMARK "OSU MPI%s Bi-Directional Bandwidth Test"
/*
 * Copyright (C) 2002-2018 the Network-Based Computing Laboratory
 * (NBCL), The Ohio State University. 
 *
 * Contact: Dr. D. K. Panda (panda@cse.ohio-state.edu)
 *
 * For detailed copyright and licensing information, please refer to the
 * copyright file COPYRIGHT in the top level OMB directory.
 */
#include <osu_util.h>

int main(int argc, char *argv[])
{
    int myid, numprocs, i, j;
    int size;
    char *s_buf, *r_buf;
    double t_start = 0.0, t_end = 0.0, t = 0.0;
    double t_sum = 0.0, t_min = 0.0, t_max = 0.0;
    int window_size = 64;
    int po_ret = 0;
    options.bench = PT2PT;
    options.subtype = BW;

    set_header(HEADER);
    set_benchmark_name("osu_bibw");
    
    po_ret = process_options(argc, argv);
    window_size = options.window_size;

    if (PO_OKAY == po_ret && NONE != options.accel) {
        if (init_accel()) {
            fprintf(stderr, "Error initializing device\n");
            exit(EXIT_FAILURE);
        }
    }

    MPI_CHECK(MPI_Init(&argc, &argv));
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &numprocs));
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &myid));
    
    if (0 == myid) {
        switch (po_ret) {
            case PO_CUDA_NOT_AVAIL:
                fprintf(stderr, "CUDA support not enabled.  Please recompile "
                        "benchmark with CUDA support.\n");
                break;
            case PO_OPENACC_NOT_AVAIL:
                fprintf(stderr, "OPENACC support not enabled.  Please "
                        "recompile benchmark with OPENACC support.\n");
                break;
            case PO_BAD_USAGE:
                print_bad_usage_message(myid);
                break;
            case PO_HELP_MESSAGE:
                print_help_message(myid);
                break;
            case PO_VERSION_MESSAGE:
                print_version_message(myid);
                MPI_CHECK(MPI_Finalize());
                exit(EXIT_SUCCESS);
            case PO_OKAY:
                break;
        }
    }

    switch (po_ret) {
        case PO_CUDA_NOT_AVAIL:
        case PO_OPENACC_NOT_AVAIL:
        case PO_BAD_USAGE:
            MPI_CHECK(MPI_Finalize());
            exit(EXIT_FAILURE);
        case PO_HELP_MESSAGE:
        case PO_VERSION_MESSAGE:
            MPI_CHECK(MPI_Finalize());
            exit(EXIT_SUCCESS);
        case PO_OKAY:
            break;
    }

    if(numprocs % 2 != 0) {
        if(myid == 0) {
            fprintf(stderr, "This test requires exactly two processes\n");
        }

        MPI_CHECK(MPI_Finalize());
        exit(EXIT_FAILURE);
    }

    int first_size = options.vary_mes_size;
    int last_size = 1;
    int scale = first_size*4;
    if (options.vary_mes_type == 0)
	last_size = options.vary_mes_size;
    options.max_message_size *= scale;
    
    int mms = options.max_message_size/scale;
    if (allocate_memory_pt2pt(&s_buf, &r_buf, myid%2)) {
        /* Error allocating memory */
        MPI_CHECK(MPI_Finalize());
        exit(EXIT_FAILURE);
    }

    print_header(myid, BW);

    if (myid == 1) {
	fprintf(stdout, "VaryMessageArgs: %d %d \n", options.vary_mes_size, options.vary_mes_type);
    }


    
    float size_scale = 1 + (float) options.mes_size_step / 100.0;
    /* Bi-Directional Bandwidth test */
    for(size = options.min_message_size; size <= options.max_message_size/scale; size *= size_scale) {
        /* touch the data */
        //set_buffer_pt2pt(s_buf, myid, options.accel, 'a', size);
        //set_buffer_pt2pt(r_buf, myid, options.accel, 'b', size);
	set_buffer_pt2pt(s_buf, myid%2, options.accel, 'a', size * first_size);
        set_buffer_pt2pt(r_buf, myid%2, options.accel, 'b', size * first_size);

        if(size > LARGE_MESSAGE_SIZE) {
            options.iterations = options.iterations_large;
            options.skip = options.skip_large;
        }

	MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
	
        if(myid < numprocs/2) {
	    int dest = numprocs/2 + myid;
	    int pairNum = myid;
	    int tagBase1 = window_size*20*pairNum;
	    int tagBase2 = window_size*20*pairNum + window_size;

	    int size_i = size;
	    if ( myid == 0 )
		size_i = size*first_size;
	    else if ( myid == numprocs/2 - 1  && size/last_size > 0)
		size_i = size/last_size;
	    
            for(i = 0; i < options.iterations + options.skip; i++) {
                if(i == options.skip) {
		    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
                    t_start = MPI_Wtime();
                }

                for(j = 0; j < window_size; j++) {
                    MPI_CHECK(MPI_Irecv(r_buf+(j*size_i)%mms, size_i, MPI_CHAR, dest, tagBase1+j, MPI_COMM_WORLD,
                            recv_request + j));
                }

                for(j = 0; j < window_size; j++) {
                    MPI_CHECK(MPI_Isend(s_buf+(j*size_i)%mms, size_i, MPI_CHAR, dest, tagBase2+j, MPI_COMM_WORLD,
                            send_request + j));
                }

                MPI_CHECK(MPI_Waitall(window_size, send_request, reqstat));
                MPI_CHECK(MPI_Waitall(window_size, recv_request, reqstat));
            }

            t_end = MPI_Wtime();
            t = t_end - t_start;

        }

        else if(myid  > numprocs/2 - 1) {
	    int dest = myid - numprocs/2;
	    int pairNum = dest;
	    int tagBase1 = window_size*20*pairNum;
	    int tagBase2 = window_size*20*pairNum + window_size;

	    int size_i = size;
	    if ( dest == 0 )
		size_i = size*first_size;
	    else if ( dest == numprocs/2 - 1 && size/last_size > 0)
		size_i = size/last_size;
	    
            for(i = 0; i < options.iterations + options.skip; i++) {
		if(i == options.skip) {
		    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
                    t_start = MPI_Wtime();
                }
                for(j = 0; j < window_size; j++) {
                    MPI_CHECK(MPI_Irecv(r_buf+(j*size_i)%mms, size_i, MPI_CHAR, dest, tagBase2+j, MPI_COMM_WORLD,
                            recv_request + j));
                }

                for (j = 0; j < window_size; j++) {
                    MPI_CHECK(MPI_Isend(s_buf+(j*size_i)%mms, size_i, MPI_CHAR, dest, tagBase1+j, MPI_COMM_WORLD,
                            send_request + j));
                }

                MPI_CHECK(MPI_Waitall(window_size, send_request, reqstat));
                MPI_CHECK(MPI_Waitall(window_size, recv_request, reqstat));
            }

	    t_end = MPI_Wtime();
            t = t_end - t_start;
        }

	MPI_CHECK(MPI_Reduce(&t, &t_sum, 1, MPI_DOUBLE, MPI_SUM, 0,
			     MPI_COMM_WORLD));
	MPI_CHECK(MPI_Reduce(&t, &t_min, 1, MPI_DOUBLE, MPI_MIN, 0,
			     MPI_COMM_WORLD));
	MPI_CHECK(MPI_Reduce(&t, &t_max, 1, MPI_DOUBLE, MPI_MAX, 0,
			     MPI_COMM_WORLD));

	MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
	double *recv_time = NULL;
	if (myid == 0)
	    recv_time = malloc(sizeof(double)*numprocs);
	
	
	//fprintf(stdout, "time,rank: %.*f,%d\n",FLOAT_PRECISION, t, myid);
	MPI_CHECK( MPI_Gather(&t, 1, MPI_DOUBLE, recv_time, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD) );
	
        if(myid == 0) {
            double tmp = size / 1e6 * options.iterations * window_size * 2;

            //fprintf(stdout, "%-*d%*.*f\n", 10, size, FIELD_WIDTH,
	    //FLOAT_PRECISION, tmp / t);

	    double t_avg = t_sum / (double) numprocs;
	    double total_bw = (tmp * ((double) numprocs/2)) / t_max;

	    fprintf(stdout, "bw-avg-min-max: %-*d%*.*f %.*f %.*f %.*f ", 10, size, FIELD_WIDTH,
                    FLOAT_PRECISION, tmp / t_avg, FLOAT_PRECISION, tmp / t_min, FLOAT_PRECISION, tmp / t_max,
		    FLOAT_PRECISION, total_bw);
            

	    for (int j = 0; j < numprocs / 2; ++j) {
		fprintf(stdout, " %.*e ", FLOAT_PRECISION, recv_time[j] / (options.iterations * window_size) );
		//fflush(stdout);
	    }
	    fprintf(stdout,"\n");
	    fflush(stdout);
        }
	if (size*size_scale < size + 1)
	    size += 1;
    }

    free_memory(s_buf, r_buf, myid);
    MPI_CHECK(MPI_Finalize());

    if (NONE != options.accel) {
        if (cleanup_accel()) {
            fprintf(stderr, "Error cleaning up device\n");
            exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}
