\title{Detailed modeling of heterogeneous and contention-constrained
  point-to-point MPI communication}
\author{Andreas Thune \and Sven-Arne Reinemo \and Tor Skeie \and Xing Cai}

\maketitle

\begin{abstract}
Heterogeneity and contention are two keywords that characterize
many-pair, point-to-point MPI communication on a heterogeneous network
topology, which has a variety of latency and bandwidth values. 
Here, heterogeneity can
also apply to the number of neighbors per process and the size per MPI message,
while contention for the bandwidth can exist among the processes.
To get a detailed understanding of the individual communication cost per
MPI process, we propose a new modeling methodology that
incorporates both heterogeneity and contention. First, we improve the existing
max-rate model to better quantify the actually achievable bandwidth
depending on the number of MPI processes in competition. Then, we make a further
extension by allowing each MPI process to have a varied number of
neighbors, with also non-uniform message sizes. Thereafter, we include
more flexibility by considering, in particular, the competition effects between
intra-socket and inter-socket communication.
Through a series of experiments done on different processor
architectures, we show that the new heterogeneous and
contention-constrained performance models can adequately explain the
individual communication cost associated with each MPI process. For a
realistic case of point-to-point MPI communication that involves 8,192
processes and in total {\color{red}\bf YYY} messages over 64 dual-socket AMD Epyc Rome
compute nodes, the overall prediction accuracy is 84\%.
\end{abstract}


\section{Introduction}

Modern platforms of parallel computing are heterogeneous at least with respect to
the interconnect. Even on a system purely based on
multicore CPUs, the connectivity between the CPU cores has several layers. The 
cores that belong to the same CPU socket can communicate very
efficiently, e.g., through a physically shared cache. The CPU
sockets on the same compute node of a cluster
 use a shared memory system, but with non-uniform access speeds. Thus, the
 inter-socket memory bandwidth, over which the cores can
 communicate across the sockets, is lower than the
 intra-socket counterpart. At the cluser level, between any pair of compute nodes, the
 communication speed is even lower and can depend on the actual
 location of the nodes on the network topology.
All these levels of interconnect heterogeneity will translate  
into vastly different values of the effective latency and bandwidth of
point-to-point MPI communication.

Another complicating factor for many-pair, point-to-point MPI
communication on today's parallel platforms is the potential competition between
different MPI processes. This is because each CPU socket can, if
needed, support a large number of concurrent MPI
processes. Contention arises when multiple pairs of sending-receiving 
processes simultaneously communicate over the same connection.
Such contention may exist, in different magnitudes,
over the entire network. Moreover, the competition situation is often dynamically changing;
for example, an MPI process pair communicating a small message may complete before the
other MPI process pairs, resulting in reduced contention.

This paper aims to detailedly model the per-process
overhead associated with realistic many-pair, point-to-point MPI communication on
modern computing platforms. We want to improve the state-of-the-art
quantitative modeling of a large number of ``heterogeneous''
MPI processes that can affect each other in point-to-point MPI
communication. Here, the heterogeneity not only reflects that the MPI
processes can run on processor cores that are heterogeneously
connected: intra-socket, inter-socket and inter-node. Moreover, 
we target the real-world situation where different MPI processes can
have different numbers of neighbors to exchange data with, while the size of
each message is highly non-uniform. One typical example of such a heterogeneous
scenario arises from numerically solving a partial differential
equation (PDE) over an irregular solution domain. The first step of
parallelizing a mesh-based PDE solver is to partition the unstructured
computational mesh, which covers the irregular solution domain, into a desirable
number of subdomains each assigned to an MPI process. The usual
partitioning result is that the number of the nearest
neighbors varies from process to process, and so does the size of each
MPI message.

Specifically, we will propose a new modeling
methodology that quantifies both heterogeneity and contention. At the
same time, we want to inherit a level of simplicity from the fundamental
``postal'' model~\cite{10.5555/576280,SPAA92_paper} that describes a
single pair of MPI processes, and the successor max-rate
model~\cite{gropp2016modeling}.  These two elegant models rely on only
two parameters to quantify the cost
of point-to-point communication, i.e., a constant start-up latency $\tau$
and a bandwidth value $BW$ that may depend on the number of competing
MPI processes in the shape of a ``roofline''. Our new performance
models are based on a principle of  fair competition among the MPI
processes, whereas the values of $\tau$ and $BW$ will
depend {\em dynamically} on the actual number of competing MPI processes and
how these processes affect each other from the different groupings:
intra-socket, inter-socket and inter-node.
The contributions of our work can be summarized as follows:
\begin{itemize}
\item We extend the {\tt osu\_bibw} micro-benchmark from MVAPICH~\cite{mvaphich} to 
  easily tabulate the various $\tau$ and $BW$ values, where both
  depend on the connection type and the latter is also a function of
  the number of
  competing MPI processes. These tabulated values serve as a characterization of the
  communication performance of a heterogeneous interconnect.
\item We improve the accuracy of the max-rate
  model~\cite{gropp2016modeling} that targets multiple pairs of MPI
  processes concurrently exchanging equal-sized
  messages. Specifically, we use the tabulated 
  $BW$ values instead of an over-simplified ``roofline''
  model for $BW$.
\item We introduce a ``staircase''  modeling strategy to handle
  many pairs of MPI processes over a single level of interconnect, but with varying numbers of
  neighbors and message sizes.
\item We extend the single-level ``staircase'' model to a mixed-level
  ``staircase'' model that also considers the interaction between
  the different communication levels:
intra-socket, inter-socket and inter-node.
\end{itemize}

% Of particular value is this modeling methodology to legacy MPI code
% which is not easily reprogrammed to adopt a mixed MPI+X style. The
% emphasis of our work is on quantifying the intra-node point-to-point
% MPI communication. Modern CPU architectures can easily give rise to
% more than 100 CPU cores per compute node (in a dual-socket setting). 

The remainder of the paper is organized as follows.
Section~\ref{sec:postAndEst} will introduce a new multi-pair ping-pong
micro-benchmark, which can be used to pinpoint the achievable
bandwidth as a function of the computing send-receive
pairs. Section~\ref{sec:model} will be devoted to a
new ``staircase'' modeling strategy that can be adopted to handle the
various types of heterogeneity, more
specifically, non-uniform message size, varied number of
sender/receivers per MPI process, and the interaction between intra-socket
and inter-socket communication. Section~\ref{sec:experiments} will
extend the ``staircase'' strategy and the resulting new models to
realistic cases of many-pair, point-to-point MPI communication that
mixes the intra-socket, inter-socket and inter-node types.
{\color{red} There needs to be a section about related work and a
  concluding section.}
