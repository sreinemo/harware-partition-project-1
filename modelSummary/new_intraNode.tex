\section{New models of many-pair, point-to-point communication} 
\label{sec:model}

Using tabulated $BW_{\mathrm{MP}}(N)$ values that are determined by the new micro-benchmark will indeed improve the accuracy of the max-rate model (\ref{eq:Tmod}), which however still requires that all the send-receive pairs communicate with the same message size. New models are thus needed to quantify the per-process time usage for the cases where the message size is non-uniform or, more generally, the number of senders/receives per process also varies. This will be the subject of this section.


\subsection{General many-pair, point-to-point communication}
First, let us use Algorithm~\ref{alg:cota} to formally define
a general scenario of many-pair, point-to-point communicaiton, where the total number of MPI processes is denoted by $N$. Here, each process with rank $0\le i<N$ will receive $M_i$ messages from $M_i$ different neighbors (one message per neighbor), and will send one message to each of the same $M_i$ neighbors. The sizes of the $M_i$ messages can be non-uniform; neither should the sizes of the receiving and sending messages match. Specifically, we have for each process two sets of message sizes $\{\textrm{Ssize}_j\}_{j=0}^{M_i-1}$ and $\{\textrm{Rsize}_j\}_{j=0}^{M_i-1}$. Each process also has a list of message destinations $\{\textrm{dest}_j\}_{j=0}^{M_i-1}$ and another list of message sources $\{\textrm{source}_j\}_{j=0}^{M_i-1}$. The resulting communication pattern can be represented by a sparse matrix, for which Figure~\ref{fig:n16} shows a simple example involving $N=16$ processes. There, each black box indicates a sender-receiver pair.

\centerline{
\begin{minipage}{0.6\textwidth}
\begin{algorithm}[H]
 \KwData{On each process $i$, $\{\textrm{Ssize}_j\}_{j=0}^{M_i-1}$, $\{\textrm{dest}_j\}_{j=0}^{M_i-1}$ , $\{\textrm{Sbuffer}_j\}_{j=0}^{M_i-1}$, $\{\textrm{Rsize}_j\}_{j=0}^{M_i -1}$, $\{\textrm{source}_j\}_{j=0}^{M_i-1}$, $\{\textrm{Rbuffer}_j\}_{j=0}^{M_i-1}$}
\For {$j=0,...M_i-1$}{
MPI\_Isend($\textrm{Sbuffer}_j$, $\textrm{Ssize}_j$, $\textrm{dest}_j$, send\_req$_j$)\;
} 
\For {$j=0...,M_i-1$}{
MPI\_Irecv($\textrm{Rbuffer}_j$, $\textrm{Rsize}_j$, $\textrm{source}_j$, recv\_req$_j$)\;
} 
\For {$j=...$}{
MPI\_Wait(send\_req$_j$)\;
}
\For {$j=...$}{
MPI\_Wait(recv\_req$_j$)\;
}
\caption{General many-pair, point-to-point communication. \label{alg:cota}}
\end{algorithm}
%\hspace{40pt}
\end{minipage}
\hspace{0.01\textwidth}
\begin{minipage}{0.45\textwidth}
\begin{figure}[H]
%\centering
\includegraphics[scale=0.3,trim={10cm 1cm 8cm 0cm},clip]{figures/cotaMatHeat.png}
\caption{An example of a communication pattern involving $N=16$ MPI processes. {\color{red} It is better to have different values in the black boxes.}}
\label{fig:n16}
\end{figure}
\end{minipage}
}

\subsection{A ``staircase'' modeling strategy}
Before developing models of increasing complexity for quantifying the per-process time usage associated with Algorithm~\ref{alg:cota}, it is necessary for us to introduce a ``staircase'' modeling strategy. The following principles are associated with this modeling strategy:
\begin{enumerate}
\item The time needed by process $i$ is determined by either how soon it can complete receiving all its $M_i$ incoming messages, or how soon all its outgoing messages are received at the $M_i$ destinations. The maximum among these time usages will apply;
\item It is unnecessary (and impossible) to determine the order in which the $M_i$ incoming messages are received.
The $M_i$ incoming messages are thus logically considered to be received with the ``same'' pace, i.e., they complete simultaneously.
\item The time needed for sending an outgoing message by process $i$ is the same as the time needed by the corresponding destination process to complete receiving all its incoming messages.
\item When multiple processes compete for the same network connection, a ``staircase'' principle will apply. To begin with, all the involved processes {\em evenly} share the bandwidth of the concerned connection. The process with the least amount of communication will complete first, letting the remaining processes to continue with their remaining communication, still evenly sharing the bandwidth. This procedure repeats itself until all the processes are completed. The name of ``staircase'' is due to the fact that the actual number of processes that compete for the same connection decreases step-wise.
\end{enumerate}

% \noindent
% \\
% \\
% The point-to-point communication described in Algorithm \ref{alg:cota} is necessary to perform a parallel SpMV operation. The applications we have considered exclusively utilize symmetric matrices, which means that for every process $i$ receives and sends the same number of messages. The model we describe in this section should, however, be capable of estimating the per-process cost of the point-to-point message operation in Algorithm \ref{alg:cota} for any given communication pattern, including when processes send more or fewer messages than they receive.
% \\
% \\
% We expand on the model in Eq. \ref{eq:Tmod} in steps. First, we only consider pairwise communication with non-uniform message sizes, including situations where messages are sent with both the eager and rendezvous protocol. Second, we consider point-to-point communication where each process sends and receives messages from multiple destinations and sources, but where all messages are either on-socket or off-socket. Lastly, we consider the case where the communication pattern induces both on- and off-socket data transfers. 

\subsection{Model for single-neighbor, non-uniform message size}

The ``staircase'' modeling strategy will be first applied to the scenario of each process having exactly one incoming message and one outgoing message, i.e., $M_i=1$ for all the processes in Algorithm~\ref{alg:cota}. In total there are $N$ unique messages, and the size of these is non-uniform. Moreover, all the messages are supposed to compete for the same network connection, and its bandwidth under contention is characterized by a set of tabulated $BW_{\mathrm{MP}}(1), BW_{\mathrm{MP}}(2), \ldots,BW_{\mathrm{MP}}(N)$ values.

Quantification of the per-process time usage will start with re-ordering the $N$ processes in an increasing order with respect to the size of the per-process single incoming message: $S=\{s_0,...,s_{N-1}\}$. Then, the per-process time usage $T^i$ can be calculated as follows:
\begin{align}
t^i &= t^{i-1} + \frac{(N-i)(s_i-s_{i-1})}{BW_{\mathrm{MP}}(N-i)}, \quad\mbox{with }s_{-1}=0, \ t^{-1}=0, \label{eq:nonUni} \\
T^i &= t^i + \tau. \label{eq:nonUniLat}
\end{align}
The numerator in the fraction in the $t^i$ expression equals the number of active receiver processes, $(N-i)$, multiplied with the amount of data process $i$ has not yet received, which is $(s_i-s_{i-1})$. This product is the total amount of data to be received by the $N-i$ remaining active receiver processes, after process $i-1$ has completed its message, until process $i$ is finished.
An estimate for the completion time of all processes would be the estimate for the last process $N-1$, which is $T^{N-1}$. The cost model in (\ref{eq:nonUni}) accounts for decreasing bandwidth contention as more and more processes complete. 
%The bandwidth term $BW_{on}(N-i)$ can be set using either a max-rate estimate, or more accurate measurements, such as the ones reported in Table \ref{tab:cavium2}. 
The cost model in (\ref{eq:nonUni}) is only valid for single-message per-process communication, but it can be extended to account for multiple message sources and destinations. This will be covered in Section~\ref{sec:3.4}

\subsubsection*{Example}

We illustrate the model (\ref{eq:nonUni})-(\ref{eq:nonUniLat}) by quantifying the per-process time usage of 6 MPI processes running on one socket of an ARM Cavium ThunderX2 processor. (Some of the on-socket $BW_{\mathrm{MP}}$ values can be found in Table~\ref{tab:cavium2}.) The 6 processes form three sender-receiver pairs, and there three message sizes are$\frac{s}{2}$, $s$ and $2s$ bytes, with $s$ ranging between 2 bytes and 2MB. The time usages for the three pairs are plotted against the model estimates in Figure~\ref{fig:3size}. The eager-rendezvous protocol threshold at 8KB is clearly visible in the plot, as there is a clear jump in execution time when the message size crosses this threshold.
\begin{figure}[h!]
\includegraphics[scale=0.4]{figures/T6D0D0.png}
\caption{Non-uniform intra-node and on-socket messages on the ARM Cavium ThunderX2 CPU. Experiment consists of 6 total processes sending messages bi-directionally in pairs. Measured execution times and model estimates are plotted with dotted and solid lines against the message size $s$. Pair 0, 1 and 2 exchange, respectively, $2s$, $s$ and $\frac{s}{2}$ bytes of data. }
\label{fig:3size}
\end{figure}

In Figure~\ref{fig:3size} we can see that the model estimates fit well for most message sizes. The model is able to estimate the transfer cost of each individual process. For each process we can observe two clear jumps in the execution time. Each jump corresponds to a change in the message protocol, first between short and eager, then between eager and rendezvous.

\subsection{Model for multi-neighbor, non-uniform message size}
\label{sec:3.4}

The model (\ref{eq:nonUni})-(\ref{eq:nonUniLat}) assumes that all communication is single-neighbor. Now let us consider a more general situation where each process communicates with multiple neighbors. The resulting new model should therefore be sufficient to handle general communication patterns when all communication happens on the same level, e.g, on-socket.

We extend the cost estimate in (\ref{eq:nonUni})  by a simple summation over all the incoming message sizes per process. Assume that we have $N$ processes each receiving multiple messages from different sources. Let $V=\{V_0,..,V_{N-1}\}$ be a sorted list, in ascending order, of the total amount of bytes each process will receive. The cost estimate in (\ref{eq:nonUni}) for single-neighbor, non-uniform message size communication can then easily be extended to model the multi-neighbor case:
\begin{align}
t^i &= t^{i-1} + \frac{(N-i)(V_i-V_{i-1})}{BW_{\mathrm{MP}}(N-i)}, \quad\mbox{with }V_{-1}=0, \ t^{-1}=0, \label{eq:multMes}\\
T^i &= t^i + {M_i}\cdot \tau.\label{eq:multMesLat}
\end{align}
The above formulas differ from (\ref{eq:nonUni})-(\ref{eq:nonUniLat}) in that the latency cost is added per message, and that the per-process communication volume $V_i$ is the sum of all its incoming messages, instead of a single message size $s_i$.

Because the process that takes the longest time to receive all its messages determines the overall execution time, the estimate for the slowest process or processes is the most important. However, the cost models in (\ref{eq:nonUni}) and (\ref{eq:multMes}) enable us to estimate the execution time of the individual processes.   When one process sends a message to another process that receives a high volume of data from other sources, the first process will be forced to "wait" for the slower process. To better estimate the completion time of all processes, we introduce a "waiting penalty". 

When messages are sent with the rendezvous protocol, we assume that the destination process receives all of its messages ``simultaneously". We estimate the potential waiting penalty induced by a receiver process $i$ onto source processes $\{\textrm{source}_j\}_{j=0}^{M_i-1}$ with a parameter $Wt_j$. We order the source processes with respect to $\mathrm{Ssize}_j$, each source sends to the receiver $i$. With this ordering and the cost estimate $T^i$, we can calculate $Wt_{j}$ by
\begin{align}
Wt_{j} &= Wt_{j-1} + \frac{(m_i -j)(Ssize_j-Ssize_{j-1})(T^i-Wt_{j-1})}{V_i^{tot}-size_{j-1}}. \label{eq:waitPen}
\end{align}
The new cost estimate $\hat{T}^{source_j}$ for the source process $\textrm{source}_j$ is then:
\begin{align}
\hat{T}^{source_j} = \max\left(T^{source_j},Wt_{j}\right).
\end{align}
If a process sends messages to multiple destinations, the largest "waiting penalty" is chosen as the process cost estimate.

\subsubsection*{Example}
To demonstrate the model in Eq. \ref{eq:multMes} and the waiting penalty in Eq. \ref{eq:waitPen}, we consider an experiment on Cavium and Kunpeng. We use a constructed communication pattern with 64 active process sending and receiving 3 on-socket messages of varying size. In Figure \ref{fig:nab3}, the per-process cost estimate is plotted against actual execution time measurements.
\begin{figure}[ht!]
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.21,trim={2.1cm 0cm 3.3cm 1.8cm},clip]{figures/cavium3nabN64core.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
\includegraphics[scale=0.21,trim={2cm 0cm 3.3cm 1.8cm},clip]{figures/kunpeng3nabN64core.png}
\subcaption{}
\end{subfigure}
%\includegraphics[scale=0.3]{figures/nabs3CoreT64.png}
\caption{Execution time (blue bar) and cost estimate (green/orange bar) plotted for each process on Cavium (a) and Kunpeng (b). The orange bar represents the waiting penalty.}
\label{fig:nab3}
\end{figure}
\\
\\
In the Figure \ref{fig:nab3} bar plot, execution time is displayed in blue, the Eq. \ref{eq:multMes} in green and the waiting penalty from Eq. \ref{eq:waitPen} in orange. Notice that the inclusion of the waiting penalty improves the per-process estimate. 
Visually, the results in Figure \ref{fig:nab3} gives the impression of good correspondence between model and measurements. To quantify model accuracy, we use the total relative average error:
\begin{align}
RelErr=\frac{\sum_{i=0}^{N-1} |t_i - est_i |}{\sum_{i=0}^{N-1} t_i}. \label{eq:err}
\end{align}
Here $t_i$ and $est_i$ represent measured time and estimated time on process $i$ for Algorithm \ref{alg:cota}. Using the total relative average error in Eq. \ref{eq:err}, we evaluate model accuracy in the Figure \ref{fig:nab3} experiments to be 0.0271 for Cavium and 0.0699 for Kunpeng.

\subsection{Mixing on- and off-socket type messages}
We extend the data transfer cost model yet again so that we are able to handle scenarios where we have processes receiving both on- and off-socket messages. Assume that each process $i$ shall receive a total of $V_i^{on}$ bytes from processes on the same socket, and $V_i^{off}$ bytes from off-socket processes. Define also the total amount of bytes to be received by process $i$ as $V_i^{tot}= V_i^{on}+V_i^{off}$ and the proportion of bytes that process $i$ will receive on-socket as $\theta_i = \frac{V_i^{on}}{V_i^{tot}}$. Using the the $\theta_i$ parameter we define the mixed bandwidth of process $i$ when there are $x$ active message receivers as:
\begin{align}
BW_{\theta_i}(x)= \theta_i BW_{on}(x) + ( 1-\theta_i )BW_{off}(x). \label{ex:mixBw}
\end{align}
If we assume the $N$ processes are numbered in the order they complete receiving all their messages, we can use the mixed bandwidth $BW_{\theta_i}$ to define an estimate for the completion time of each process:
\begin{align}
t^i &= t^{i-1} +  \frac{(N-i)(V_i^{tot}-R_i^{i})}{BW_{\theta_i}(N-i)}, \quad t^{-1}=0 \\ \label{eq:mix1}
T^i &= t^i +  \sum_{M_i^{on}} \tau_{on} + \sum_{M_i^{off}} \tau_{off},\\
R_{j}^{i} & = R_{j}^{i-1}+\frac{BW_{\theta_j}(N-i)(t^i -t^{i -1})}{N-i}, \quad \textrm{for $j=i+1,...,N-1$.} \label{eq:mix2}
\end{align}
The $R_{j}^{i}$ term is introduced to keep track of how much data each process has received at a given time. $R_{j}^{i}$ is calculated for all processes that have not completed receiving its messages. 
For each step in the "staircase" model the data added to the $R_{j}^{i}$ term equals the per process bandwidth $\frac{BW_{\theta_j}(N-i)}{N-i}$ multiplied with the estimated cost of the i-th step, which is $t^i -t^{i -1}$.
The term must be computed for each process because $\theta_j$, and therefore also $BW_{\theta_j}$, varies between each process. 
\subsubsection*{Example}
We illustrate the mixed message type estimate in Eq. \ref{eq:mix1}-\ref{eq:mix2} with two simple examples, one on a Cavium node, and the other on a Kunpeng node. In the first experiment we use a total of 48 processes, where 32 processes send and receive on-socket messages (16 on each socket) and 16 processes send and receive off-socket messages (8 on each socket). The results are presented in Figure \ref{fig:mixOnOff}a, and the experiment commuication pattern is presented in Figure \ref{fig:mixOnOff}b. Notice that the model correctly estimates that the off-socket processes complete after the on-socket processes. Using the error measure in Eq. \ref{eq:err}, we achieve a model accuracy of 0.064. 
\\
\\
In the Kunpeng experiment we use the same communication pattern as in the experiment presented in Figure \ref{fig:nab3}, but to introduce a mix of on- and off-socket communication, we run our experiment with the OpenMPI option \texttt{-map-by socket}. The per-process message size and type is displayed in Figure \ref{fig:mixOnOff}d. Experiment execution time and model estimate is displayed in Figure \ref{fig:mixOnOff}c. We achieve a model accuracy of 0.035.
\begin{figure}[ht!]
\begin{subfigure}{.55\textwidth}
\includegraphics[scale=0.25,trim={2cm 0cm 3cm 0cm},clip]{figures/mixOnOffN48.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.25,trim={10cm 0cm 2cm 0cm},clip]{figures/on-off-heat.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.55\textwidth}
\includegraphics[scale=0.25,trim={2cm 0cm 3cm 0cm},clip]{figures/on-off-hua.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.25,trim={1cm 0cm 1cm 0cm},clip]{figures/on-off-hua-comVol.png}
\subcaption{}
\end{subfigure}
\caption{Plot in (a) shows execution time (blue) and cost estimate (green) for an experiment on Cavium using 48 processes, with the cpmmunication pattern shown in the plot in (b). The plot in (c) shows execution time and cost estimate for an experiment on Kunpeng where we use 64 processes. In this example each process sends and receives three messages of varying size. The amount of on- and off-socket data each process receives is displayed in the plot in (d).}%48 processes sending and receiving either on-socket or off-socket messages on an ARM Cavium node. Process 0-15 and 32-47 send/receive messages of on-socket type, while process 16-31 send and receive off-socket messages. The blue bars represent measured completion time, while the green bars represent estimated time.}
\label{fig:mixOnOff}
\end{figure}

\subsection{Mixing intra- and inter-node messages}
If we use more than one node, we will typically see both intra- and inter-node communication. The inter-node communication can be modelled similar to the intra-node case, but the bandwidth is different and less sensitive to the number of active receiver processes. We therefore use the expression in \ref{eq:multMes}-\ref{eq:multMesLat} to model the per-process inter-node data transfer cost. 
\begin{align}
t_{on}^i &= t_{inter}^{i-1} + \frac{(N-i)(V_i-V_{i-1})}{BW_{inter}(N-i)}, \quad V_{-1}=0, t_{inter}^{-1}=0, \label{eq:inter}\\
T_{inter}^i &= t_{inter}^i + \sum_{M_i} \tau_{inter}\label{eq:interLat}
\end{align}
When a process receives both intra- and inter-node messages we estimate the total cost to be the sum of the inter and intra-node estimates. We also assume that the intra-node and inter-node estimates do not impact each other. The per-process cost estimate for process $i$ is given in expression Eq. \ref{eq:inter}.
\begin{align}
T^{i}_{total}= T^{i}_{intra} + T^{i}_{inter}. \label{eq:inter}
\end{align}
We have assumed that inter-node bandwidth is only restricted by interconnect hardware, but on large clusters, network topology can also play a part. We will not consider this in this work.

\iffalse
%%%% COMMENT OUT DIFFERENT PROTOCOLS %%%%
%%%% COMMENT OUT DIFFERENT PROTOCOLS %%%%
\subsubsection*{Different protocols}
Because the MPI protocol depends on the message size, multiple message sizes can and will result in messages being sent with different protocols. We therefore need to consider the case where we send both eager and rendezvous messages. Assume that we have $N_e$ processes receiving messages with the eager protocol and $N_r$ processes receiving rendezvous messages. The eager and rendezvous processes will only affect each other if the rendezvous latency is lower than the total execution time of the eager pairs. We therefore need to estimate how much data the eager pairs are able to communicate during the rendezvous latency. The per process data received by the eager pairs, $V_e$, is introduced and calculated as:
\begin{align}
V_e = \frac{(\tau_{L,r}-\tau_{L,e})BW_{L,e}(N_e)}{N_e}. \label{eq:nuV1}
\end{align}
If $V_e$ is larger than the size of each individual eager pair, the rendezvous pairs will have no impact on the eager pairs. However, if $V_e$ is smaller than some of the eager pairs, eager and rendezvous pairs will be active at the same time. The impact of the rendezvous pairs can be represented by modifying the expression in Eq. \ref{eq:nonUni} for the eager protocol. We first introduce an index $P$ for protocol in the time, bandwidth and latency terms, such as $T_{L,P}^i$. The impact of the rendezvous processes on the eager processes is introduced by replacing eager latency $\tau_{L,e}$ with rendezvous latency $\tau_{L,r}$ and setting $s_0=V_e$, as well as counting the rendezvous processes in the bandwidth term: 
\begin{align}
t_{L,eager}^i &= t_{L,eager}^{i-1} + \frac{(N_e+N_r-i)(s_i-s_{i-1})}{BW_{L,P}(N_e +N_r-i)}, \quad s_{-1}=V_e, T_{L,eager}^{-1}=0, \\
T_{L,eager}^i &= t_{L,eager}^i + \tau_{L,rend}  \label{eq:TmodE}
\end{align}
%T_{L,eager}(N_e,S) &= \tau_{L,r} + \sum_{i=0}^{N_e-1}\frac{(N_e-i)(s_{i+1}-s_{i})}{BW_{L,e}(N_e+N_r-i)\frac{N_e}{N_e+N_r}},\quad s_0=V_e.
If the eager protocol pairs remains active longer than the rendezvous latency, they will impact the rendezvous pairs. We therefore need to estimate how much data the rendezvous pairs are able to send before the eager protocol pairs complete. We introduce the $V_r$ term, for estimating rendezvous per pair data sent while eager pairs are active as:
\begin{align}
V_r = \max\left( \frac{(T_{L,eager}^{N_e-1}-\tau_{L,r})BW_{L,r}(N_e+N_r)}{N_e+N_r}, 0 \right). \label{eq:nuV1}
\end{align}
The impact of eager pairs on the rendezvous pairs are introduced into the general non-uniform message cost expression in Eq. \ref{eq:nonUni}, by setting $s_0=V_r$ and replacing the rendezvous latency with $\max(\tau_{L,r},T_{L,e})$:
\begin{align}
t_{L,rend}^i &= t_{L,rend}^{i-1} + \frac{(N_r-i)(s_i-s_{i-1})}{BW_{L,P}(N_r-i)}, \quad s_{-1}=V_r, T_{L,rend}^{-1}=0, \\
T_{L,P}^i &= t_{L,P}^i + \max(\tau_{L,r},T_{L,e}^{N_e-1}). \label{eq:TmodR} 
\end{align}
%T_{L,rend}(N,S) &= \max(\tau_{L,r},T_{L,e})  + \sum_{i=0}^{N_r-1}\frac{(N_r-i)(s_{i+1}-s_{i})}{BW_{L,P}(N_r-i)},\quad s_{0}=V_r. 
If $V_r=0$ the expression in Eq. \ref{eq:TmodR} is equal to the one in Eq. \ref{eq:nonUni}. The modified non-uniform message cost models in Eq. \ref{eq:TmodE} and \ref{eq:TmodR} are only needed when both the eager and rendezvous protocols are used to send messages. When this is not the case the cost model in Eq. \ref{eq:Tmod2} is sufficient.
%%%% END COMMENT OUT DIFFERENT PROTOCOLS %%%%
%%%% END COMMENT OUT DIFFERENT PROTOCOLS %%%%
\fi