add_executable("partition-CpGrid" partition-CpGrid.cc)
target_link_dune_default_libraries("partition-CpGrid")

set (HAVE_ECL_INPUT 1)
set (ECL_INPUT 1)


add_executable("readDeck"
  readDeck.cpp
  io/readDeck.hpp
  io/initClass.hpp
  io/vtkWrite.hpp
  io/comTabInfo.hpp
  io/edgeCutInfo.hpp
  comm/createComm.hpp
  comm/timeComm.hpp)
if (FALSE)
  add_executable("overlapMatrix"
    overlapMatrix.cpp
    io/readDeck.hpp
    io/initClass.hpp
    io/vtkWrite.hpp
    io/comTabInfo.hpp
    io/edgeCutInfo.hpp
    comm/createComm.hpp
    comm/timeComm.hpp)

  add_executable("bw_bench"
    bw_bench.cpp
    mpi_bench/help_methods.hpp
    mpi_bench/transfer_methods.hpp)


  set(my_EXECUTABLES
    readDeck
    bw_bench
    overlapMatrix
  )
endif()

add_executable("simpleCpGrid"
  simpleCpGridPlat.cpp)

add_executable("artificialComm"
  artificialComm.cpp
  comm/createComm.hpp
  comm/timeComm.hpp
  comm/setNab.hpp)

set(my_EXECUTABLES
  readDeck
  simpleCpGrid
  artificialComm)


#find_package(Zoltan CONFIG REQUIRED)
find_package(opm-common CONFIG REQUIRED)
find_package(opm-grid CONFIG REQUIRED)
find_package(opm-material CONFIG REQUIRED)
#find_package(ewoms CONFIG REQUIRED)
find_package(opm-simulators CONFIG REQUIRED)



foreach(exe ${my_EXECUTABLES})
  
  target_link_libraries(${exe} ${opm-simulators_LIBRARIES})
  target_link_libraries(${exe} ${opm-grid_LIBRARIES})     
  target_link_libraries(${exe} ${opm-common_LIBRARIES})

  target_link_libraries(${exe} ${Boost_LIBRARIES})	
  target_link_libraries(${exe} ${Boost_FILESYSTEM_LIBRARIES})
  target_link_libraries(${exe} ${Boost_REGEX_LIBRARIES})
  
  target_link_libraries(${exe} ${MPI_LIBRARIES})
  
endforeach()
