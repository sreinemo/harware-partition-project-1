/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_INITCLASS_HEADER_INCLUDED
#define OPM_INITCLASS_HEADER_INCLUDED

#endif // OPM_INITCLASS_HEADER_INCLUDED


class ReadInit
{
public:
  
    std::map<int,std::string> dict;

    void read_file_and_update(char* init)
    {
	std::ifstream myfile {init};
	
	std::string b;
	int a;
	while (myfile>> a>>b)
	{
	    dict[a]=b;
	}
    }
    void write_param()
    {
	for (int i=0; i<dict.size(); ++i)
	{
	    std::cout <<i<<": "<<dict[i] <<" ";
	}
	std::cout<<std::endl;
    }

    ReadInit ()
    {
	dict[0]  = std::string("1");    //Use trans
	dict[1]  = std::string("1");    //Overlap Layers
    }
};
