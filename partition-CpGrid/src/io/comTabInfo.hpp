/*
  Copyright 2020 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_COMTABINFO_HEADER_INCLUDED
#define OPM_COMTABINFO_HEADER_INCLUDED

#endif // OPM_COMTABINFO_HEADER_INCLUDED

template<class G, class C>
void findComTab(const G& grid, std::vector<int>& comTab, const C& cc )
{
    int rank = cc.rank();
    int pSize = cc.size();
    
    comTab.resize(pSize, 0);

    const auto& rid = grid.getCellRemoteIndices();

    for (auto && r : rid)
    {
	int ranki = r.first;
	for(auto && rr : *r.second.first)
	{
	    auto glob = rr.localIndexPair().global();
	    auto ra = rr.attribute();
	    auto la = rr.localIndexPair().local().local();//.attribute();
	    if (rr.localIndexPair().local().attribute() == Dune::OwnerOverlapCopyAttributeSet::copy) {
		comTab[ranki]++;
	    }
	    //if (rank == 0)
	    //std::cout << "rank: "<< ranki << " la: " << la << " glob: "<<glob<<std::endl;
	    //info.addRemoteIndex(std::tuple<int,int,int>(rank,glob,ra));
	}
    }
}


template<class C>
void printComTab(const C& cc, std::vector<int>& comTab)
{
    //--- Send ---
    int size = cc.size();
    int rank = cc.rank();
    
    int tabSize = size*size;

    int* sendTab = comTab.data();
    int entireTab[tabSize];

    int start[size];
    int length[size];
    for (int i = 0; i < size; ++i) {
	start[i] = i*size;
	length[i] = size;
    }

    cc.gatherv(sendTab, size, entireTab, length, start, 0);

    //--- Print ---

    if (rank == 0) 
    {
	std::cout << std::endl;
	
	for (int prc = 0; prc < size; ++prc) 
	{
	    std::cout << "ComTab rank " << prc <<": ";
	    for ( int nab = 0; nab < size; ++nab)
	    {
		int idx = prc*size + nab;
		std::cout << entireTab[idx] << " ";
	    }
	    std::cout << std::endl;
	}
    }
}

template<class G, class C>
void findAndPrintComTab(const G& grid, std::vector<int>& comTab, const C& cc )
{
    findComTab(grid, comTab, cc);
    printComTab(cc, comTab);
}

template<class G, class C>
int printCellInfo(const G& grid, const C& cc)
{
    int numCells = grid.numCells();
    int numInterior = 0;
    int numGhost = 0;

    auto gv = grid.leafGridView();

    for (auto&& cell : elements(gv)) {
	if (cell.partitionType() == Dune::InteriorEntity)
	    numInterior++;
	else
	    numGhost++;
    }

    int size = cc.size();
    int rank = cc.rank();

    int cellTab[size];
    int interiorTab[size];
    int ghostTab[size];

    cc.gather(&numCells, cellTab, 1 , 0);
    cc.gather(&numInterior, interiorTab, 1 , 0);
    cc.gather(&numGhost, ghostTab, 1 , 0);

    if (rank == 0) 
    {
	std::cout << std::endl;
	
	for (int prc = 0; prc < size; ++prc) 
	{
	    std::cout << "Rank " << prc <<" ghostCells: " << ghostTab[prc]<<std::endl;
	    
	}

	std::cout << std::endl;
	
	for (int prc = 0; prc < size; ++prc) 
	{
	    std::cout << "Rank " << prc <<" numCells: " << cellTab[prc]<<std::endl;
	    
	}

	std::cout << std::endl;
	
	for (int prc = 0; prc < size; ++prc) 
	{
	    std::cout << "Rank " << prc <<" interiorCells: " << interiorTab[prc]<<std::endl;
	    
	}
    }

    return numGhost;
}
