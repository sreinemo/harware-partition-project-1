 Data for JOB [19132,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n004	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [19132,1] App: 0 Process rank: 0 Bound: socket 0[core 25[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [19132,1] App: 0 Process rank: 1 Bound: socket 0[core 18[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../BB/../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 5.9731e-06 5.9742e-06 
dune-size 2 5.34621e-06 5.34701e-06 
dune-size 4 5.61281e-06 5.61501e-06 
dune-size 8 6.43781e-06 6.43281e-06 
dune-size 16 6.56001e-06 6.55901e-06 
dune-size 32 7.84851e-06 7.84321e-06 
dune-size 64 1.06981e-05 1.06923e-05 
dune-size 128 1.60258e-05 1.60239e-05 
dune-size 256 1.74505e-05 1.7452e-05 
dune-size 512 3.02807e-05 3.02846e-05 
dune-size 1024 8.58757e-05 8.58615e-05 
dune-size 2048 0.00016468 0.000164723 
dune-size 4096 0.000314463 0.000314541 
dune-size 8192 0.000556874 0.000556875 
dune-size 16384 0.000926968 0.000926969 
dune-size 32768 0.00163951 0.00163951 
dune-size 65536 0.00331394 0.00331395 
dune-size 131072 0.00635274 0.00635385 
dune-size 262144 0.0126659 0.0126654 
dune-size 524288 0.0252472 0.0252493 
dune-size 1048576 0.0506546 0.0506178 
