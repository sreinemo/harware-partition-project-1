 Data for JOB [24128,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n004	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [24128,1] App: 0 Process rank: 0 Bound: socket 1[core 49[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../BB/../../../../../../../../../../../../../..]
 	Process OMPI jobid: [24128,1] App: 0 Process rank: 1 Bound: socket 1[core 42[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../BB/../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 5.6148e-06 5.6181e-06 
dune-size 2 5.3732e-06 5.37e-06 
dune-size 4 5.56121e-06 5.55951e-06 
dune-size 8 6.51981e-06 6.52091e-06 
dune-size 16 6.558e-06 6.5568e-06 
dune-size 32 7.83641e-06 7.83891e-06 
dune-size 64 1.15514e-05 1.15467e-05 
dune-size 128 1.5711e-05 1.57307e-05 
dune-size 256 1.73967e-05 1.74007e-05 
dune-size 512 3.06358e-05 3.06235e-05 
dune-size 1024 8.49947e-05 8.4989e-05 
dune-size 2048 0.000162165 0.000162156 
dune-size 4096 0.000313942 0.000313941 
dune-size 8192 0.00057793 0.000577936 
dune-size 16384 0.000869655 0.000869649 
dune-size 32768 0.00158764 0.00158761 
dune-size 65536 0.00324256 0.00324266 
dune-size 131072 0.00629895 0.00629793 
dune-size 262144 0.0126045 0.0126083 
dune-size 524288 0.0251382 0.0251382 
dune-size 1048576 0.0502242 0.0502216 
