 Data for JOB [22990,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n004	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [22990,1] App: 0 Process rank: 0 Bound: socket 1[core 41[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../BB/../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [22990,1] App: 0 Process rank: 1 Bound: socket 0[core 26[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 8.63641e-06 8.62811e-06 
dune-size 2 8.08131e-06 8.08291e-06 
dune-size 4 9.21711e-06 9.21901e-06 
dune-size 8 7.88411e-06 7.88241e-06 
dune-size 16 7.45401e-06 7.45371e-06 
dune-size 32 8.99571e-06 8.99321e-06 
dune-size 64 1.15432e-05 1.1542e-05 
dune-size 128 1.65531e-05 1.65717e-05 
dune-size 256 1.95121e-05 1.95092e-05 
dune-size 512 3.34607e-05 3.34614e-05 
dune-size 1024 0.000111898 0.00011193 
dune-size 2048 0.000205866 0.000205861 
dune-size 4096 0.00038636 0.000386412 
dune-size 8192 0.00066795 0.000667875 
dune-size 16384 0.000923874 0.000923859 
dune-size 32768 0.00166336 0.00166335 
dune-size 65536 0.00334069 0.0033377 
dune-size 131072 0.0066745 0.00667197 
dune-size 262144 0.01329 0.0132884 
dune-size 524288 0.0264129 0.0263864 
dune-size 1048576 0.0508472 0.0508434 
