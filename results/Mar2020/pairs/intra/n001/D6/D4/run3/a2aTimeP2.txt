 Data for JOB [2422,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n001	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [2422,1] App: 0 Process rank: 0 Bound: socket 1[core 49[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../BB/../../../../../../../../../../../../../..]
 	Process OMPI jobid: [2422,1] App: 0 Process rank: 1 Bound: socket 1[core 34[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 4.19429e-06 4.19469e-06 
dune-size 2 4.00009e-06 4.00159e-06 
dune-size 4 4.1463e-06 4.1476e-06 
dune-size 8 4.32889e-06 4.32819e-06 
dune-size 16 5.44788e-06 5.45218e-06 
dune-size 32 5.60059e-06 5.60059e-06 
dune-size 64 7.22119e-06 7.21949e-06 
dune-size 128 1.06019e-05 1.0606e-05 
dune-size 256 1.76398e-05 1.76485e-05 
dune-size 512 3.0644e-05 3.06542e-05 
dune-size 1024 8.63347e-05 8.63149e-05 
dune-size 2048 0.000159181 0.00015922 
dune-size 4096 0.000307774 0.000307765 
dune-size 8192 0.000515447 0.000515452 
dune-size 16384 0.000870541 0.000870509 
dune-size 32768 0.00164051 0.00164054 
dune-size 65536 0.00325253 0.00325233 
dune-size 131072 0.00638973 0.00639013 
dune-size 262144 0.0128301 0.0128329 
dune-size 524288 0.0254332 0.0254316 
dune-size 1048576 0.0505562 0.0505568 
