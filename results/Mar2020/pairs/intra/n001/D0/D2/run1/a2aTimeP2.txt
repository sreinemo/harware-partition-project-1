 Data for JOB [39996,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n001	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [39996,1] App: 0 Process rank: 0 Bound: socket 0[core 1[hwt 0-1]]:[../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [39996,1] App: 0 Process rank: 1 Bound: socket 0[core 18[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../BB/../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 4.13249e-06 4.13209e-06 
dune-size 2 3.94488e-06 3.94488e-06 
dune-size 4 4.02689e-06 4.02569e-06 
dune-size 8 4.99418e-06 4.99648e-06 
dune-size 16 4.69468e-06 4.69508e-06 
dune-size 32 5.45979e-06 5.45989e-06 
dune-size 64 7.16868e-06 7.16928e-06 
dune-size 128 1.04602e-05 1.04617e-05 
dune-size 256 1.73303e-05 1.73263e-05 
dune-size 512 3.06357e-05 3.06345e-05 
dune-size 1024 8.49748e-05 8.49662e-05 
dune-size 2048 0.000156177 0.000156184 
dune-size 4096 0.000302322 0.00030231 
dune-size 8192 0.00055007 0.000548431 
dune-size 16384 0.000865939 0.000865891 
dune-size 32768 0.00162433 0.00162427 
dune-size 65536 0.0032337 0.00323233 
dune-size 131072 0.00640738 0.00640743 
dune-size 262144 0.0128171 0.0128134 
dune-size 524288 0.0255179 0.0254872 
dune-size 1048576 0.0509605 0.0509519 
