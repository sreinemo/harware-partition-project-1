 Data for JOB [45462,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n001	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [45462,1] App: 0 Process rank: 0 Bound: socket 1[core 33[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [45462,1] App: 0 Process rank: 1 Bound: socket 0[core 26[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 5.58899e-06 5.59169e-06 
dune-size 2 5.41139e-06 5.41519e-06 
dune-size 4 5.51548e-06 5.51558e-06 
dune-size 8 5.74458e-06 5.74468e-06 
dune-size 16 6.62588e-06 6.62748e-06 
dune-size 32 7.10218e-06 7.10688e-06 
dune-size 64 8.78148e-06 8.78378e-06 
dune-size 128 1.22461e-05 1.22553e-05 
dune-size 256 1.93493e-05 1.93506e-05 
dune-size 512 3.31538e-05 3.31612e-05 
dune-size 1024 0.000105273 0.000105291 
dune-size 2048 0.000198748 0.000198736 
dune-size 4096 0.000380057 0.00038003 
dune-size 8192 0.000627623 0.000627549 
dune-size 16384 0.000923655 0.000923498 
dune-size 32768 0.00167781 0.00167776 
dune-size 65536 0.00331496 0.00331426 
dune-size 131072 0.00653817 0.00653779 
dune-size 262144 0.013209 0.0131946 
dune-size 524288 0.026051 0.0260198 
dune-size 1048576 0.0516601 0.0516399 
