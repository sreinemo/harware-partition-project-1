 Data for JOB [4943,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n001	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [4943,1] App: 0 Process rank: 0 Bound: socket 1[core 57[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../../..]
 	Process OMPI jobid: [4943,1] App: 0 Process rank: 1 Bound: socket 1[core 42[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../BB/../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 4.10149e-06 4.10319e-06 
dune-size 2 3.87029e-06 3.86929e-06 
dune-size 4 5.04009e-06 5.03779e-06 
dune-size 8 4.28579e-06 4.28509e-06 
dune-size 16 4.67549e-06 4.67339e-06 
dune-size 32 5.46328e-06 5.46358e-06 
dune-size 64 7.20398e-06 7.20428e-06 
dune-size 128 1.04792e-05 1.04816e-05 
dune-size 256 1.74674e-05 1.74712e-05 
dune-size 512 3.03973e-05 3.03925e-05 
dune-size 1024 8.66251e-05 8.66278e-05 
dune-size 2048 0.000156321 0.000156333 
dune-size 4096 0.00030607 0.000306073 
dune-size 8192 0.000547939 0.000547916 
dune-size 16384 0.00088355 0.00088356 
dune-size 32768 0.00164706 0.0016471 
dune-size 65536 0.00324241 0.00324289 
dune-size 131072 0.00635362 0.00635555 
dune-size 262144 0.0128588 0.0128602 
dune-size 524288 0.0254889 0.025501 
dune-size 1048576 0.0507652 0.0508136 
