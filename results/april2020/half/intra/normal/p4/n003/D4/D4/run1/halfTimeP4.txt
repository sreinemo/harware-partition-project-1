 Data for JOB [5861,1] offset 0 Total slots allocated 4

 ========================   JOB MAP   ========================

 Data for node: n003	Num slots: 4	Max slots: 0	Num procs: 4
 	Process OMPI jobid: [5861,1] App: 0 Process rank: 0 Bound: socket 1[core 33[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [5861,1] App: 0 Process rank: 1 Bound: socket 1[core 35[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [5861,1] App: 0 Process rank: 2 Bound: socket 1[core 36[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [5861,1] App: 0 Process rank: 3 Bound: socket 1[core 34[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
1 0 0
2 3 3
3 2 2
Two group cota
0 1 1
dune-size 1 2.75989e-06 2.76319e-06 3.52819e-06 3.52889e-06 
dune-size 2 2.55849e-06 2.56319e-06 3.31109e-06 3.31569e-06 
dune-size 4 2.71189e-06 2.71599e-06 3.44299e-06 3.44549e-06 
dune-size 8 6.60829e-06 6.61109e-06 7.46808e-06 7.46878e-06 
dune-size 16 3.27759e-06 3.28069e-06 4.07559e-06 4.07709e-06 
dune-size 32 4.05249e-06 4.05659e-06 4.92939e-06 4.93339e-06 
dune-size 64 5.55819e-06 5.56089e-06 6.52589e-06 6.52729e-06 
dune-size 128 8.65539e-06 8.65919e-06 9.72528e-06 9.72818e-06 
dune-size 256 1.48347e-05 1.48383e-05 1.63748e-05 1.63767e-05 
dune-size 512 2.70419e-05 2.7045e-05 2.90356e-05 2.90501e-05 
dune-size 1024 5.58757e-05 5.58831e-05 7.54499e-05 7.54509e-05 
dune-size 2048 0.000107095 0.000107097 0.000133909 0.000133898 
dune-size 4096 0.000209673 0.000209677 0.000259548 0.000259527 
dune-size 8192 0.000404691 0.000404699 0.000467818 0.000467815 
dune-size 16384 0.000795668 0.000795668 0.000827598 0.00082758 
dune-size 32768 0.00156467 0.00156463 0.001615 0.00161509 
dune-size 65536 0.0031858 0.00318559 0.00321459 0.00321711 
dune-size 131072 0.00645117 0.0064502 0.0064661 0.00647192 
