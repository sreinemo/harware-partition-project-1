 Data for JOB [51641,1] offset 0 Total slots allocated 8

 ========================   JOB MAP   ========================

 Data for node: n003	Num slots: 8	Max slots: 0	Num procs: 8
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 0 Bound: socket 1[core 32[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 1 Bound: socket 1[core 33[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 2 Bound: socket 1[core 34[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 3 Bound: socket 1[core 35[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 4 Bound: socket 0[core 12[hwt 0-1]]:[../../../../../../../../../../../../BB/../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 5 Bound: socket 0[core 13[hwt 0-1]]:[../../../../../../../../../../../../../BB/../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 6 Bound: socket 0[core 14[hwt 0-1]]:[../../../../../../../../../../../../../../BB/../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [51641,1] App: 0 Process rank: 7 Bound: socket 0[core 15[hwt 0-1]]:[../../../../../../../../../../../../../../../BB/../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
Two group cota
0 1 3
0 2 2
0 3 1
1 2 0
1 3 3
1 0 2
3 0 2
3 1 1
3 2 0
2 3 1
2 0 0
2 1 3
5 6 4
5 7 7
5 4 6
6 7 5
6 4 4
6 5 7
7 4 6
7 5 5
7 6 4
4 5 7
4 6 6
4 7 5
dune-size 1 1.22576e-05 1.22808e-05 1.4449e-05 1.23006e-05 1.26543e-05 1.2666e-05 1.48407e-05 1.27121e-05 
dune-size 2 6.52639e-06 6.53589e-06 6.53179e-06 6.53019e-06 8.25258e-06 8.24178e-06 8.25568e-06 8.24108e-06 
dune-size 4 6.91899e-06 6.92589e-06 6.92789e-06 6.92069e-06 6.92279e-06 6.92629e-06 6.92569e-06 6.92789e-06 
dune-size 8 8.51278e-06 8.50958e-06 8.51968e-06 8.51088e-06 1.08192e-05 1.08111e-05 1.08141e-05 1.08105e-05 
dune-size 16 8.65338e-06 8.64108e-06 8.65138e-06 8.64428e-06 8.66188e-06 8.67898e-06 8.67018e-06 8.68868e-06 
dune-size 32 1.16566e-05 1.16866e-05 1.16748e-05 1.16662e-05 1.10553e-05 1.10696e-05 1.10579e-05 1.10719e-05 
dune-size 64 1.59054e-05 1.59374e-05 1.5932e-05 1.59176e-05 1.6567e-05 1.65797e-05 1.6553e-05 1.65586e-05 
dune-size 128 2.4953e-05 2.49351e-05 2.49373e-05 2.4954e-05 2.49633e-05 2.49339e-05 2.49403e-05 2.49355e-05 
dune-size 256 4.33042e-05 4.33264e-05 4.33316e-05 4.33187e-05 4.33491e-05 4.34119e-05 4.33692e-05 4.33518e-05 
dune-size 512 8.00252e-05 8.00668e-05 8.00465e-05 8.00337e-05 8.08205e-05 8.06356e-05 8.07119e-05 8.06315e-05 
dune-size 1024 0.000170399 0.000170391 0.000170397 0.000170391 0.000175553 0.000175518 0.000175602 0.000175513 
dune-size 2048 0.00032111 0.00032111 0.000321548 0.000321772 0.000328347 0.00032793 0.00032793 0.000328644 
dune-size 4096 0.000616714 0.000617552 0.00061671 0.000617796 0.000624687 0.000624272 0.000624262 0.000625483 
dune-size 8192 0.00122398 0.00122134 0.00122134 0.00122273 0.00122846 0.00122896 0.00122679 0.00122678 
dune-size 16384 0.00236333 0.00236328 0.00236458 0.00236774 0.00242655 0.00242959 0.00242314 0.00242313 
dune-size 32768 0.00484763 0.00484753 0.00484744 0.00484734 0.00493669 0.00493204 0.00492518 0.00492508 
dune-size 65536 0.00983442 0.00983143 0.0098319 0.00983019 0.00967905 0.00972677 0.00967906 0.00968622 
dune-size 131072 0.0196893 0.0196906 0.0196889 0.0196897 0.0198273 0.0194209 0.0196274 0.0194199 
